module.exports = {
    "parserOptions": {
        "ecmaVersion": 5,
        "sourceType": "script"
    },
    "extends": [
        "google",
        "plugin:backbone/recommended"
    ],
    "rules": {
        "indent": ["error", 4],
        "max-len": ["error", 150],
        "no-var": "off",
        "comma-dangle": ["error", "never"]
    },
    "globals": {
        "jQuery": true,
        "$": true
     },
    "env": {
        "browser": true
    }
};