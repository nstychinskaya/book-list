require.config({
    paths: {
        'Book': '/js/book.model',
        'ListOfBooks': 'js/books.collection',
        'BooksTable': 'js/books.table.view',
        'EditBookForm': 'js/editBook.form.view',
        'BooksTableRow': 'js/books.table-row.view'
    }
});

require(
    ['Book', 'ListOfBooks', 'BooksTable', 'EditBookForm'],
    function(Book, ListOfBooks, BooksTable, EditBookForm) {
        var list = new ListOfBooks();
        list.fetch();

        var booksTable = new BooksTable({
            el: $('#books-table'),
            collection: list
        });

        var editBookForm = new EditBookForm({
            el: $('#edit-book')
        });

        editBookForm.listenTo(booksTable, 'show-form', function(book) {
            if (book) {
                this.setBook(book);
                this.showForm();
            }
        });

        list.listenTo(editBookForm, 'save-book', function(newBook) {
            this.create(newBook);
            editBookForm.hideForm();
        });
        list.listenTo(editBookForm, 'update-book', function(model, newBook) {
            model.set(newBook).save();
            this.localStorage.update(model);
            editBookForm.hideForm();
        });
    }
);
