define(function() {
    return Backbone.Model.extend({
        defaults: {
            title: 'no-title',
            author: 'no-author'
        }
    });
});
