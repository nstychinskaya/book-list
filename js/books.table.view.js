define(['BooksTableRow'], function(BooksTableRow) {
    return Backbone.View.extend({
        initialize: function() {
            this.render();
            this.listenTo(this.collection, 'all', this.render);
        },
        render: function() {
            this.$el.html('');
            this.collection.forEach(function(model) {
                var row = new BooksTableRow({model: model});

                row.on('edit-book', function(model) {
                    this.trigger('show-form', model);
                }, this);

                this.$el.append(row.render().el);
            }.bind(this));
            return this;
        }
    });
});
