define(function() {
    return Backbone.View.extend({
        events: {
            'click .delete-link': 'delete',
            'click .edit-link': 'edit'
        },
        template: Handlebars.compile($('#books-row-tpl').html()),
        tagName: 'tr',
        render: function() {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        },
        delete: function() {
            this.model.destroy();
        },
        edit: function() {
            this.trigger('edit-book', this.model);
        }
    });
});
