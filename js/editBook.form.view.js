define(function() {
    return Backbone.View.extend({
        events: {
            'click .open-add-book-form': 'addBook',
            'click .cancel-btn': 'hideForm',
            'submit #edit-book-form': 'saveBook'
        },
        template: Handlebars.compile($('#edit-book-form-tpl').html()),
        render: function() {
            console.log(this.model);
            var html = this.template({formHeader: 'Create or update book'});
            this.$el.find('#edit-book-form').html(html);
            if(this.model) {
                this.$el.find('input[name="bookTitle"]').val(this.model.get('title'));
                this.$el.find('input[name="bookAuthor"]').val(this.model.get('author'));
            }
            return this;
        },
        setBook: function(book) {
            this.model = book;
        },
        showForm: function() {
            this.render();
            this.$el.find('#edit-book-form').show();
        },
        hideForm: function() {
            this.$el.find('#edit-book-form').hide();
        },
        addBook: function() {
            this.model = undefined;
            this.showForm();
        },
        saveBook: function(event) {
            event.preventDefault();
            var tmpBook = {
                title: this.$el.find('input[name="bookTitle"]').val(),
                author: this.$el.find('input[name="bookAuthor"]').val()
            };
            this.model ? this.trigger('update-book', this.model, tmpBook) : this.trigger('save-book', tmpBook);
        }
    });
});
